# FederationDemo

Demonstrates webpack 5 Module Federation with Angular and the Angular Router.

## Start

- Install dependencies with yarn or npm
You can choose to run MFE 1 or 2, or both, this will allow the main project to use any of them
- Run Micro Frontend 1
  - ng serve mfe1 -o
- Run Micro Frontend 2
  - ng serve mfe2 -o
